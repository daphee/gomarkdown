package gomarkdown

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

type TreeElement interface {
	Attach(TreeElement) TreeElement
	SetRoot(TreeElement)
	GetRoot() TreeElement
	GetChildren() []TreeElement
	GetName() string
	SetName(string)
	SetStart(int)
	GetStart() int
	ToHTML(*bytes.Buffer)
}

type BaseElement struct {
	children []TreeElement
	root     TreeElement
	name     string
	startPos int
}

func (e *BaseElement) Attach(t TreeElement) TreeElement {
	//fmt.Printf("Attaching %v to %v\n", t, e)
	e.children = append(e.children, t)
	t.SetRoot(e)
	return t
}

func (e *BaseElement) GetChildren() []TreeElement {
	return e.children
}

func (e *BaseElement) SetRoot(t TreeElement) {
	e.root = t
}

func (e *BaseElement) GetRoot() TreeElement {
	return e.root
}

func (e *BaseElement) SetName(s string) {
	e.name = s
}

func (e *BaseElement) GetName() string {
	return e.name
}

func (e *BaseElement) SetStart(i int) {
	e.startPos = i
}

func (e *BaseElement) GetStart() int {
	return e.startPos
}

func (e *BaseElement) ToHTML(out *bytes.Buffer) {
	switch e.GetName() {
	case "root":
		for _, c := range e.GetChildren() {
			c.ToHTML(out)
		}

	case "paragraph":
		out.WriteString("<p>")
		for _, c := range e.GetChildren() {
			c.ToHTML(out)
		}
		out.WriteString("</p>")

		if e.GetRoot().GetChildren()[len(e.GetRoot().GetChildren())-1] != e {
			out.WriteString("\n\n")
		}

	case "codeblock":
		out.WriteString("<pre><code>")
		for _, c := range e.GetChildren() {
			c.ToHTML(out)
		}
		out.WriteString("</code></pre>")

		if e.GetRoot().GetChildren()[len(e.GetRoot().GetChildren())-1] != e {
			out.WriteString("\n\n")
		}

	case "block":
		out.WriteString("<br>")
	}
}

type ContentElement struct {
	BaseElement
	content []byte
}

func (e *ContentElement) SetContent(s []byte) {
	e.content = s
}

func (e *ContentElement) GetContent() []byte {
	return e.content
}

func (e *ContentElement) ToHTML(out *bytes.Buffer) {
	if len(e.GetContent()) == 0 {
		return
	}

	switch e.GetName() {
	case "plain":
		out.Write(e.GetContent())

	case "emphasis":
		out.WriteString("<em>")
		out.Write(e.GetContent())
		out.WriteString("</em>")

	case "emphasis_strong":
		out.WriteString("<strong>")
		out.Write(e.GetContent())
		out.WriteString("</strong>")
	}
}

type HeaderElement struct {
	BaseElement
	level      int
	parseUntil int
}

func (e *HeaderElement) SetLevel(l int) {
	e.level = l
}

func (e *HeaderElement) GetLevel() int {
	return e.level
}

func (e *HeaderElement) ToHTML(out *bytes.Buffer) {
	out.WriteString("<h" + strconv.Itoa(e.GetLevel()) + ">")
	for _, c := range e.GetChildren() {
		c.ToHTML(out)
	}
	out.WriteString("</h" + strconv.Itoa(e.GetLevel()) + ">\n\n")
}

func (e *HeaderElement) SetParseUntil(i int) {
	e.parseUntil = i
}

func (e *HeaderElement) GetParseUntil() int {
	return e.parseUntil
}

type BufferElement struct {
	BaseElement
}

func (e *BufferElement) ToHTML(out *bytes.Buffer) {
	for _, c := range e.GetChildren() {
		c.ToHTML(out)
	}
}

//should be used for underlinings with '=' and '-'
func isUnderlinedWith(input []byte, p int, char byte) int {
	length := len(input)
	if p+1 < length && input[p+1] == char {
		pp := p + 1
		for pp < length && (input[pp] == ' ' || input[pp] == '\t' || input[pp] == char) {
			pp++
		}
		if pp < length && input[pp] == '\n' {
			return pp + 1
		}
	}

	return 0
}

func isIndented(input []byte, p int) bool {
	countSpaces := 0
	//At least for spaces or one tab
	length := len(input)
	for i := 1; i < 5; i++ {
		if p+i < length && input[p+i] == ' ' {
			countSpaces++
		} else if p+1 < length && input[p+i] == '\t' {
			return true
		}
	}
	return countSpaces == 4
}

func Parse(input []byte) *BaseElement {
	p := 0

	root := &BaseElement{}
	root.SetName("root")

	var current TreeElement

	length := len(input)

	currentLine := &BufferElement{}
	currentLine.SetName("buffer")
	currentLine.SetRoot(root)
	currentLine.SetStart(0)
	current = currentLine
	for p <= length {

		//Ignore some portion of atx style headers
		if currentLine.GetRoot().GetName() == "header" {
			h := currentLine.GetRoot().(*HeaderElement)
			until := h.GetParseUntil()

			if until != 0 && p >= until && input[p] != '\n' {
				if p == until {
					if el, ok := current.(*ContentElement); ok {
						el.SetContent(input[el.GetStart():p])
					}
				}
				p++
				continue
			}
		}

		//Don't Parse codeblocks
		if currentLine.GetRoot().GetName() == "codeblock" {
			if input[p] != '\n' {
				p++
				continue
			}
		}

		//The same work of appending a line and closing open elements is done on EACH line
		//end line included where only the job to create a new one doesn't need to be done
		if p < length && input[p] == '\n' || p == length {
			//Breaks at the end are possible so check if the last element is a ContentElement
			last, ok := current.(*ContentElement)
			if ok {
				if last.GetName() == "emphasis" || last.GetName() == "emphasis_strong" {
					last.SetName("plain")
				}

				if last.GetContent() == nil {
					if p < length {
						last.SetContent(input[last.GetStart() : p+1])
					} else {
						//p+1 would throw an error
						last.SetContent(input[last.GetStart():p])
					}
				}
			}

			/*
			 *Where should the currentLine get appended?
			 */
			if pos1, pos2 := isUnderlinedWith(input, p, '='), isUnderlinedWith(input, p, '-'); pos1 != 0 || pos2 != 0 {
				header := &HeaderElement{}
				header.SetName("header")
				header.SetStart(currentLine.GetStart())

				if pos1 != 0 {
					header.SetLevel(1)
					//move the pointer after the underlining
					p = pos1
				} else if pos2 != 0 {
					header.SetLevel(2)
					p = pos2
				}

				root.Attach(header)
				current = header

				//remove the newline
				lastLen := len(last.GetContent())
				if ok && last.GetName() == "plain" && last.GetContent()[lastLen-1] == '\n' {
					last.SetContent(last.GetContent()[:lastLen-1])
				}
			} else if currentLine.GetRoot().GetName() == "root" {
				//empty line preceding, therefore check for code block
				if isIndented(input, p) {
					codeblock := &BaseElement{}
					codeblock.SetName("codeblock")
					codeblock.SetStart(currentLine.GetStart())

					current = root.Attach(codeblock)
				} else {
					paragraph := &BaseElement{}
					paragraph.SetName("paragraph")
					paragraph.SetStart(currentLine.GetStart())

					current = root.Attach(paragraph)
				}
			} else if currentLine.GetRoot().GetName() == "codeblock" && !isIndented(input, p) {
				paragraph := &BaseElement{}
				paragraph.SetName("paragraph")
				paragraph.SetStart(currentLine.GetStart())

				current = root.Attach(paragraph)
			} else if currentLine.GetRoot().GetName() == "paragraph" || currentLine.GetRoot().GetName() == "header" {
				current = currentLine.GetRoot()
				//fmt.Println("We already have a paragraph")
			}
			//Append the current line
			current.Attach(currentLine)

			if p < length {
				//Create new line
				currentLine = &BufferElement{}
				currentLine.SetName("buffer")
				currentLine.SetStart(p + 1)
				if current.GetName() == "header" {
					//Header breaks document flow
					currentLine.SetRoot(root)
				} else if current.GetName() == "paragraph" || current.GetName() == "codeblock" {
					//Append the next line to the same paragraph
					//Or codeblock,
					currentLine.SetRoot(current)
				}
				current = currentLine
			} else {
				break
			}

			//Omit empty lines
			pp := p + 1
			for pp < length && (input[pp] == ' ' || input[pp] == '\t') {
				pp++
			}

			//did we really hit a empty line or just a e.g. indented line?
			if pp < length && input[pp] == '\n' {
				//we did, omit further \n's and \r's
				for pp < length && input[pp] == '\n' {
					pp++
				}

				//fmt.Printf("Skipped from %d to %d\n", p, pp)

				if pp < length {
					//if we have a plain element last it has \n in its content. remove it
					wasInCodeBlock := last.GetRoot().GetRoot().GetName() == "codeblock"
					if !wasInCodeBlock && ok && last.GetName() == "plain" && last.GetContent()[len(last.GetContent())-1] == '\n' {
						last.SetContent(input[last.GetStart():p])
					}
					//close open element
					currentLine.SetRoot(root)
					currentLine.SetStart(pp)
					//we are currently at a content char. at the end p gets incremented so decrement it here
					p = pp - 1
				}
			}

		} else if input[p] == '*' || input[p] == '_' {
			if current.GetName() == "emphasis" || current.GetName() == "emphasis_strong" {
				if current.GetName() == "emphasis" && input[current.GetStart()] == input[p] {
					current.(*ContentElement).SetContent(input[current.GetStart()+1 : p])
					current = current.GetRoot()

					//Cheap solution to hard problem:
					//If the last word in a line is emphasised the next char in main loop while be \n
					//this \n will be in no element, therefore create a plain which starts immediately after this
					plain := &ContentElement{}
					plain.SetName("plain")
					plain.SetStart(p + 1)
					current = current.Attach(plain)
				} else if current.GetName() == "emphasis_strong" && input[current.GetStart()] == input[p] && p+1 < length &&
					input[current.GetStart()] == input[p+1] {

					current.(*ContentElement).SetContent(input[current.GetStart()+2 : p])
					current = current.GetRoot()
					p++

					plain := &ContentElement{}
					plain.SetName("plain")
					plain.SetStart(p + 1)
					current = current.Attach(plain)
				} else {
					//wrong closed emphasis
					current.SetName("plain")
				}
			} else {
				strong := false
				if p+1 < length && input[p+1] == input[p] {
					strong = true
				}
				emph := &ContentElement{}
				emph.SetStart(p)
				if current.GetName() == "plain" {
					current.(*ContentElement).SetContent(input[current.GetStart():p])
				}
				current = currentLine.Attach(emph)
				if strong {
					emph.SetName("emphasis_strong")
					p++
				} else {
					emph.SetName("emphasis")
				}
			}
		} else if input[p] == '#' && input[p-1] == '\n' {
			num := 1
			p++
			for p < length && input[p] == '#' && num < 6 {
				p++
				num++
			}
			pp := p + 1
			if p < length {
				//if loop before didn't end because we reached end of input
				//decrease the pointer to let the loop process the current byte normally
				p--
			}

			header := &HeaderElement{}
			header.SetName("header")
			header.SetStart(p)
			header.SetLevel(num)

			contentSet := false
			for pp < length && input[pp] != '\n' {
				if !contentSet && input[pp] == '#' {
					header.SetParseUntil(pp)
					contentSet = true
				} else if contentSet && input[pp] != '#' {
					//content already set but a byte different from #
					contentSet = false
				}
				pp++
			}

			if !contentSet {
				header.SetParseUntil(pp)
			}

			//headers always are at top level
			root.Attach(header)

			//and break the document flow
			currentLine.SetRoot(header)
		} else {
			if current.GetName() == "buffer" {
				//we don't know what to do with this byte so stick it in a plaintext element
				plain := &ContentElement{}
				plain.SetName("plain")
				plain.SetStart(p)
				current = currentLine.Attach(plain)
			}
		}

		p++
	}

	return root
}

func Print(t TreeElement, indent int) {
	fmt.Print(strings.Repeat(" ", indent))
	fmt.Println(t.GetName())
	if t.GetName() == "plain" || t.GetName() == "emphasis_strong" || t.GetName() == "emphasis" {
		fmt.Print(strings.Repeat(" ", indent+1))
		c := t.(*ContentElement)
		fmt.Printf("'%s'\n", string(c.GetContent()))
	}
	for _, c := range t.GetChildren() {
		Print(c, indent+1)
	}
}

//expands tabs and normalize newlines
func Preprocess(input []byte, out *bytes.Buffer) {
	length := len(input)
	out.Grow(length)

	p := 0
	for p < length {
		pp := p
		for pp < length && input[pp] != '\r' && input[pp] != '\n' && input[pp] != '\t' {
			pp++
		}

		out.Write(input[p:pp])
		if pp < length && input[pp] == '\t' {
			for i := 0; i < 4; i++ {
				out.WriteByte(' ')
			}

			pp++
		} else {
			out.WriteByte('\n')

			if pp < length && input[pp] == '\r' {
				pp++
			}

			if pp < length && input[pp] == '\n' {
				pp++
			}
		}

		p = pp
	}
}
