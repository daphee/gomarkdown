package parser

import (
	"bytes"
	//"fmt"
)

type EmphasisElement struct {
	Type    bool
	Content []byte
}

type EmphasisParser struct{}

func NewEmphasisParser() ElementParser {
	return &EmphasisParser{}
}

func (p *EmphasisElement) Attach(child Element) {}

func (p *EmphasisElement) GetName() string { return "emphasis" }

func (p *EmphasisElement) ToHTML(indent int) []byte {
	var html bytes.Buffer
	if p.Type {
		html.WriteString("<em>")
	} else {
		html.WriteString("<strong>")
	}
	html.Write(p.Content)
	if p.Type {
		html.WriteString("</em>")
	} else {
		html.WriteString("</strong>")
	}
	return html.Bytes()

}

func (p *EmphasisElement) IsBlock() bool { return false }

func (p *EmphasisParser) GetName() string { return "emphasis" }

func (p *EmphasisParser) IsBlock() bool { return false }

func (p *EmphasisParser) Parse(root Element, input []byte) int {
	//type true means em
	t := true
	if input[0] != '*' && input[0] != '_' {
		return 0
	}

	if input[1] == input[0] {
		t = false
	}

	end := 1
	if !t {
		end = 2
	}

	for end < len(input)-1 {
		if t {
			if input[end] == input[0] {
				break
			}
		} else {
			if input[end] == input[0] && input[end-1] == input[0] {
				break
			}
		}
		end++
	}

	if input[end] != input[0] || !t && input[end-1] != input[0] {
		return 0
	}
	content := input[1:end]
	if !t {
		content = input[2 : end-1]
	}

	el := &EmphasisElement{Content: content, Type: t}
	root.Attach(el)

	return end + 1
}
