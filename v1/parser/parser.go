package parser

import (
//"fmt"
)

var Parsers = Registry.Get()

func Parse(root Element, input []byte) {
	//fmt.Println("Parsing:", string(input))
	cplain := &PlaintextElement{Content: []byte{}}
	//Attach for later use to be before the current parsed element
	root.Attach(cplain)
	plainBegin := 0
	for i := 0; i < len(input); {
		for pnum, p := range Parsers {
			num := p.Parse(root, input[i:])

			//match? could the parser work on the byte
			if num > 0 {
				//fmt.Printf("%s took care of %d bytes for %s\n", p.GetName(), num, root.GetName())
				//push the byte pointer forward
				i += num

				//if the element just parsed was a block skip all empty lines
				if p.IsBlock() {
					for i < len(input) && (input[i] == '\n' || input[i] == ' ') {
						i++
					}

					if i == len(input) {
						break
					}

					root.Attach(&PlaintextElement{Content: []byte{'\n', '\n'}})
				}
				//generate new plaintext element
				cplain = &PlaintextElement{Content: []byte{}}
				root.Attach(cplain)
				//and push the plaintext pointer to the current position
				plainBegin = i
				break
			} else if pnum == len(Parsers)-1 {
				//The last parser couldn't work with this neither
				i++
				cplain.Content = input[plainBegin:i]
				//fmt.Printf("Plaintext in %s:'%s'\n", root.GetName(), input[plainBegin:i])
			}
		}
	}
}
