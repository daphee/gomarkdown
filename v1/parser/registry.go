package parser

import ()

const (
	//Block elements
	PRIORITY_PARAGRAPH = iota
	//Inline elements
	PRIORITY_EMPHASIS = iota
	PRIORITY_BREAK    = iota
)

var Registry = ElementRegistry{
	parsers: map[int]ElementParser{
		PRIORITY_PARAGRAPH: NewParagraphParser(),
		PRIORITY_EMPHASIS:  NewEmphasisParser(),
		PRIORITY_BREAK:     NewBreakParser(),
	},
}

type ElementRegistry struct {
	parsers map[int]ElementParser
}

/*type ByPriority []*Element

func (a ByPriority) Len() int           { return len(a) }
func (a ByPriority) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByPriority) Less(i, j int) bool { return a[i].Priority < a[j].Priority }*/

/*
func (r *ElementRegistry) Register(p *ElementParser, priority int) {
	parsers, ok := r.parsers[priority]
	if !ok {
		parsers = []*ElementParser{}
		r.parsers[priority] = parsers
	}

	parsers = append(parsers, e)
}*/

func (r *ElementRegistry) Get() []ElementParser {
	//sort.Sort(sort.Reverse(ByPriority(r.elements)))
	parsers := []ElementParser{}
	for _, p := range r.parsers {
		parsers = append(parsers, p)
	}
	return parsers
}
