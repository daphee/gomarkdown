package parser

import ()

type BreakElement struct{}

type BreakParser struct{}

func NewBreakParser() ElementParser {
	return &BreakParser{}
}

func (p *BreakElement) Attach(child Element) {}

func (p *BreakElement) GetName() string { return "break" }

func (p *BreakElement) ToHTML(indent int) []byte {
	return []byte("<br>")
}

func (p *BreakElement) IsBlock() bool { return false }

func (p *BreakParser) GetName() string { return "break" }

func (p *BreakParser) IsBlock() bool { return false }

func (p *BreakParser) Parse(root Element, input []byte) int {
	if len(input) < 3 || input[0] != ' ' || input[1] != ' ' || input[2] != '\n' {
		return 0
	}

	root.Attach(&BreakElement{})

	return 2
}
