package parser

import (
	"bytes"
	//"fmt"
)

type ParagraphElement struct {
	children []Element
}

type ParagraphParser struct{}

func NewParagraphParser() ElementParser {
	return &ParagraphParser{}
}

func (p *ParagraphElement) Attach(child Element) {
	p.children = append(p.children, child)
}

func (p *ParagraphElement) GetName() string { return "paragraph" }

func (p *ParagraphElement) ToHTML(indent int) []byte {
	html := bytes.Buffer{}
	html.Write([]byte("<p>"))
	for _, child := range p.children {
		html.Write(child.ToHTML(indent + 1))
	}
	html.Write([]byte("</p>"))
	return html.Bytes()
}

func (p *ParagraphElement) IsBlock() bool { return true }

func (p *ParagraphParser) GetName() string { return "paragraph" }

func (p *ParagraphParser) IsBlock() bool { return true }

func (p *ParagraphParser) Parse(root Element, input []byte) int {
	end := 0
	content_end := 0

	for end < len(input) {
		for end < len(input) && input[end] != '\n' {
			end++
		}
		if end == len(input) {
			return 0
		}
		content_end = end
		end++
		for end < len(input) && input[end] == ' ' {
			end++
		}
		if end == len(input) {
			return 0
		} else if input[end] != '\n' {
			end++
		} else {
			break
		}
	}

	if end == len(input) || input[end] != '\n' {
		return 0
	}

	paragraph := &ParagraphElement{children: []Element{}}
	root.Attach(paragraph)

	//Buggy? Remove empty line at start
	start := 0
	/*if input[0] == '\n' {
		start = 1
	}*/

	Parse(paragraph, input[start:content_end])

	return end + 1
}
