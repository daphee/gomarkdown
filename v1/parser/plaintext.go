package parser

import ()

type PlaintextElement struct {
	Content []byte
}

//type PlaintextParser struct{}

func (p *PlaintextElement) Attach(child Element) {
}

func (p *PlaintextElement) GetName() string { return "plaintext" }

func (p *PlaintextElement) ToHTML(indent int) []byte {
	return p.Content
}

func (p *PlaintextElement) IsBlock() bool { return false }
