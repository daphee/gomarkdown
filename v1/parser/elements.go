package parser

type Element interface {
	ToHTML(int) []byte
	Attach(Element)
	IsBlock() bool
	GetName() string
}

type ElementParser interface {
	Parse(Element, []byte) int
	IsBlock() bool
	GetName() string
}
