package gomarkdown

import (
	"bitbucket.org/daphee/gomarkdown/parser"
	//"fmt"
)

import (
	"bytes"
	"strings"
)

const (
	TAB_SIZE_DEFAULT = 4
)

type Markdown struct {
	Flags int
}

func New() *Markdown {
	return &Markdown{}
}

type reference struct {
	url, title string
}

type parse_session struct {
	refs map[string]*reference
	root *RootElement
}

type RootElement struct {
	children []parser.Element
}

func NewRoot() *RootElement {
	return &RootElement{children: []parser.Element{}}
}

func (r *RootElement) ToHTML(indent int) []byte {
	var html bytes.Buffer
	for _, ch := range r.children {
		html.Write(ch.ToHTML(indent + 1))
	}
	return html.Bytes()
}

func (r *RootElement) Attach(e parser.Element) {
	r.children = append(r.children, e)
}

func (r *RootElement) IsBlock() bool   { return false }
func (r *RootElement) GetName() string { return "root" }

func preprocess(p *parse_session, input []byte, out *bytes.Buffer) {
	input = []byte(strings.Replace(
		strings.Replace(string(input), "\r\n", "\n", -1),
		"\r", "\n", -1))
	/*Expand tabs*/
	input = []byte(strings.Replace(string(input), "\t", strings.Repeat(" ", TAB_SIZE_DEFAULT), -1))
	/*extract references and copy content*/
	var pointer, endpointer int

	for pointer < len(input) {
		if is, i := isReference(p, input[pointer:]); is {
			//isReference returns the index in the input slice it worked on last
			pointer += i + 1
		} else {
			endpointer = pointer
			//search line end
			for endpointer < len(input) {
				if input[endpointer] == '\n' {
					endpointer++
					break
				}
				endpointer++
			}
			out.Write(input[pointer:endpointer])
			pointer = endpointer
		}
	}
	//Add Final blank line
	/*out.WriteByte('\n')
	if input[len(input)-1] != '\n' {
		out.WriteByte('\n')
	}*/

}

func (m *Markdown) Render(input []byte) []byte {
	p := &parse_session{refs: make(map[string]*reference), root: NewRoot()}
	out := &bytes.Buffer{}
	preprocess(p, input, out)
	parser.Parse(p.root, out.Bytes())
	return p.root.ToHTML(0)
}

func isReference(p *parse_session, input []byte) (bool, int) {
	begin := 0
	//up to three whitespaces are allowed
	for begin < 3 && begin < len(input) && input[begin] == ' ' {
		begin++
	}
	if begin >= len(input) || input[begin] != '[' {
		return false, 0
	}

	begin++ //point to first character of id

	end := begin + 1
	//search for end of id in same line
	for end < len(input) && input[end] != ']' && input[end] != '\n' {
		end++
	}

	if input[end] != ']' || input[end+1] != ':' {
		return false, 0
	}

	id := strings.ToLower(string(input[begin:end]))

	begin = end + 1
	//any number of whitespaces allowed
	for begin < len(input) && input[begin] == ' ' {
		begin++
	}

	//reached end of line without finding url
	if begin >= len(input) || input[begin] == '\n' {
		return false, 0
	}

	end = begin + 2
	//search for end of url
	for end < len(input) && input[end] != ' ' && input[end] != '\n' {
		end++
	}

	url := string(input[begin:end])

	//search optional title
	newline := false
	var newlineIndex int

	begin = end + 1
	for begin < len(input) && (input[begin] == ' ' || input[begin] == '\n') {
		if input[begin] == '\n' && !newline {
			newline = true
			newlineIndex = begin
		}
		begin++
	}

	//have we reached a title
	if begin >= len(input) || (input[begin] != '\'' && input[begin] != '"' && input[begin] != '(') {
		//if we reached newline while searching for title return newline position
		if newline {
			p.refs[id] = &reference{url: url}
			return true, newlineIndex
		} else {
			return false, 0
		}
	}

	terminitionChar := input[begin]
	end = begin + 1
	for end < len(input) && input[end] != '\n' && input[end] != terminitionChar {
		end++
	}

	//have we found a valid title?
	if end >= len(input) || input[end] == '\n' {
		return false, 0
	}

	title := string(input[begin:end])

	p.refs[id] = &reference{url: url, title: title}
	return true, end
}
