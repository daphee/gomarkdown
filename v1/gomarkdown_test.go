package gomarkdown

import (
	"bytes"
	"fmt"
	"testing"
)

func TestRender(t *testing.T) {
	output := string(New().Render([]byte(`PARAGRAPH1

Paragraph with _emphasis_in it

PARA2`)))
	fmt.Println("Output:", output)
	t.Fail()
}

func TestPreprocess(t *testing.T) {
	p := &parse_session{refs: make(map[string]*reference)}
	inputNoRef := []byte(`Before
[1]: http://example.com/ failure
    "tittle"`)
	outNoRef := &bytes.Buffer{}

	preprocess(p, inputNoRef, outNoRef)

	if bytes.Compare(inputNoRef, outNoRef.Bytes()) != 0 {
		t.Fail()
	}

	if len(p.refs) != 0 {
		t.Fail()
	}

	p = &parse_session{refs: make(map[string]*reference)}
	inputRef := []byte(`Before
[2]: http://example.com/
    "tittle" aftertitle`)
	expectedOutRef := []byte(`Before
 aftertitle`)
	outRef := &bytes.Buffer{}

	preprocess(p, inputRef, outRef)

	if bytes.Compare(outRef.Bytes(), expectedOutRef) != 0 {
		t.Fail()
	}

	if len(p.refs) != 1 {
		t.Fail()
	}
}
