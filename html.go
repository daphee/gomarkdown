package gomarkdown

var blockTags = map[string]bool{
	"address":    true,
	"figcaption": true,
	"article":    true,
	"figure":     true,
	"output":     true,
	"aside":      true,
	"footer":     true,
	"p":          true,
	"audio":      true,
	"form":       true,
	"pre":        true,
	"blockquote": true,
	"h1":         true,
	"h2":         true,
	"h3":         true,
	"h4":         true,
	"h5":         true,
	"h6":         true,
	"section":    true,
	"canvas":     true,
	"header":     true,
	"table":      true,
	"div":        true,
	"hr":         true,
	"ul":         true,
	"fieldset":   true,
	"noscript":   true,
	"video":      true,
}
